import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../models/product.dart';

class HttpHelper {
  final String server = 'shoplistnghiadoan.mocklab.io';
  final String path = 'product';
  final String postPath = 'product';
  final String putPath = 'product';
  final String deletePath = 'product';

  Future<List<Product>> getProductList() async {
    Uri url = Uri.https(server, path);
    http.Response result = await http.get(url);
    if (result.statusCode == HttpStatus.ok) {
      final jsonResponse = json.decode(result.body);
      //provide a type argument to the map method to avoid type error
      List<Product> products =
          jsonResponse.map<Product>((i) => Product.fromJson(i)).toList();
      return products;
    } else {
      return [];
    }
  }

  Future<http.Response> postProduct(Product product) {
    String post = json.encode(product.toJson());
    Uri url = Uri.https(server, postPath);

    return http.post(
      url,
      body: post,
    );
  }

  Future<http.Response> putProduct(Product product) async {
    String put = json.encode(product.toJson());
    Uri url = Uri.https(server, putPath);

    return http.put(
      url,
      body: put,
    );
  }

  Future<http.Response> deleteProduct(String id) async {
    Uri url = Uri.https(server, deletePath);
    return http.delete(url, body: id);
  }
}
