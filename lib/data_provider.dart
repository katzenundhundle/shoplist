import 'package:flutter/material.dart';
import '../controller/Product_controller.dart';

class DataProvider extends InheritedWidget {
  final _controller = ProductController();

  DataProvider({Key? key, required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => false;

  static ProductController of(BuildContext context) {
    DataProvider provider = context
        .dependOnInheritedWidgetOfExactType<DataProvider>() as DataProvider;

    return provider._controller;
  }
}
