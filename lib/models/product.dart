class Product {
  late String id;
  late String name;
  late double price;
  Product(this.id, this.name, this.price);
  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      json['id'] as String,
      json['name'] as String,
      json['price'] as double,
    );
  }

  Product.createDefault() {
    id = '';
    name = '';
    price = 0;
  }

  Map<String, dynamic> toJson() {
    return {'id': id, 'name': name, 'price': price};
  }
}
