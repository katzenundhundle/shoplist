import 'package:shop_list/models/product.dart';

import '../utils/http_helper.dart';

class ProductController {
  Future<List<Product>> getProducts() async {
    HttpHelper helper = HttpHelper();
    List<Product> products = await helper.getProductList();

    return products;
  }

  callPizzas() {}
}
