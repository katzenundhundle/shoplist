// TODO Implement this library.
import 'dart:convert';

import 'package:flutter/material.dart';
import '../models/product.dart';
import '../utils/http_helper.dart';

class NewProduct extends StatefulWidget {
  late Product product;

  NewProduct({Key? key, required this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NewProductState();
  }
}

class NewProductState extends State<NewProduct> {
  final formKey = GlobalKey<FormState>();

  var nameController = TextEditingController();
  var priceController = TextEditingController();

  @override
  void initState() {
    var p = widget.product;
    nameController.text = p.name;
    priceController.text = (p.price != 0) ? p.price.toString() : '';
  }

  String saveResult = '';
  @override
  Widget build(BuildContext context) {
    var p = widget.product;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushReplacementNamed('/');
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text(p.id == "" ? ' product' : 'Update product'),
      ),
      body: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              _message(),
              _fieldName(),
              _fieldPrice(),
              _buttonSave(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _message() {
    return Text(saveResult);
  }

  Widget _fieldName() {
    return TextFormField(
      controller: nameController,
      decoration: InputDecoration(
        labelText: 'Name',
      ),
    );
  }

  Widget _fieldPrice() {
    return TextFormField(
      controller: priceController,
      keyboardType: TextInputType.numberWithOptions(),
      decoration: InputDecoration(
        labelText: 'Price',
      ),
    );
  }

  // Widget _fieldPrize() {
  //   return TextFormField(
  //     controller: priceController,
  //     keyboardType: TextInputType.number,
  //     decoration: InputDecoration(labelText: 'Price'),
  //   );
  // }

  Widget _buttonSave() {
    return ElevatedButton(
        onPressed: () {
          _saveProduct();
        },
        child: Text('Save'));
  }

  void _saveProduct() async {
    var httpHelper = HttpHelper();
    var product = widget.product;
    product.id = identityHashCode(product).toString();
    product.name = nameController.text;
    product.price = (priceController.text.isNotEmpty
        ? double.parse(priceController.text)
        : null)!;

    var jsonProduct = product.toJson();
    print(jsonProduct);

    var response;
    if (product.id == null) {
      //
      response = await httpHelper.postProduct(product);

      if (response.statusCode == 201) {
        print('Save the product successfully.');
        print('Response message= ${response.body}');
      } else {
        print('Could not save the product.');
        print('Response message= ${response.body}');
      }
    } else {
      // Update
      response = await httpHelper.putProduct(product);

      if (response.statusCode == 200) {
        print('Update the product successfully.');
        print('Response message= ${response.body}');
      } else {
        print('Could not update the product.');
        print('Response message= ${response.body}');
      }
    }
    setState(() {
      saveResult = jsonDecode(response.body)['message'];
    });
  }
}
