import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import '../views/new_product_view.dart';
import '../data_provider.dart';
import '../models/product.dart';
import '../utils/http_helper.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var productController = DataProvider.of(context);
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Home'),
      //   actions: [
      //     ElevatedButton(
      //         onPressed: () {
      //           _changeModeDelete(context);
      //         },
      //         child: Text('Delete mode')),
      //   ],
      // ),
      body: Container(
        child: FutureBuilder(
          future: productController
              .getProducts(), // callProducts(), //readJsonFile(context),
          builder:
              (BuildContext context, AsyncSnapshot<List<Product>> products) {
            return ListView.builder(
              itemCount: products.data?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                var itemData = products.data![index];
                return ListTile(
                  title: Text(itemData.name),
                  subtitle: Text("${itemData.price}"),
                  onTap: () {
                    _editProduct(context, itemData);
                  },
                );
              },
            );
          },
        ),
      ),
    );
  }

  Future<List<Product>> readJsonFile(context) async {
    String myString = await DefaultAssetBundle.of(context)
        .loadString('assets/product_list.json');

    List myMap = jsonDecode(myString);
    List<Product> myProducts = [];
    myMap.forEach((dynamic product) {
      Product myProduct = Product.fromJson(product);
      // Product myProduct = Product.fromJsonOrNull(product);
      myProducts.add(myProduct);
    });

    return myProducts;
  }

  void _editProduct(context, Product product) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => NewProduct(product: product)));
  }

  void _changeModeDelete(BuildContext context) {
    Navigator.pushReplacementNamed(context, '/list_product');
  }
}// TODO Implement this library.