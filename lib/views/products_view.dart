import 'package:flutter/material.dart';

import '../data_provider.dart';
import '../models/product.dart';
import '../utils/http_helper.dart';

class ProductsListScreen extends StatelessWidget {
  const ProductsListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var productController = DataProvider.of(context);
    return Scaffold(
      body: FutureBuilder(
          future: productController.getProducts(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Product>> products) {
            return ListView.builder(
              itemCount: products.data?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                var itemData = products.data![index];
                return Dismissible(
                  key: Key(itemData.id.toString()),
                  child: ListTile(
                    title: Text(itemData.name),
                    subtitle: Text(" price: ${itemData.price}"),
                  ),
                  onDismissed: (item) {
                    // HttpHelper httpHelper = HttpHelper();
                    // httpHelper.deleteProduct(item);
                  },
                );
              },
            );
          }),
    );
  }
}
