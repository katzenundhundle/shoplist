import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import '../../lib/models/product.dart';
import '../../lib/utils/http_helper.dart';

void main() {
  var httpHelper = HttpHelper();
  test('Test get list of products.', () async {
    var productsList = await httpHelper.getProductList();

    expect(productsList.isEmpty, false);

    for (var product in productsList) {
      print(product.name);
    }
  });

  test('Test post product to server.', () async {
    var product = Product("A1", "RTX 3080", 1200);
    var jsonproduct = product.toJson();
    print(jsonproduct);

    var response = await httpHelper.postProduct(product);

    expect(response.statusCode, 201);
    var jsonBody = jsonDecode(response.body);
    expect(jsonBody['message'], 'add product success');
  });

  test('Test put product to server.', () async {
    var product = Product("A2", "RTX 3080", 1200);

    var response = await httpHelper.putProduct(product);

    expect(response.statusCode, 200);
    var jsonBody = jsonDecode(response.body);
    expect(jsonBody['message'], 'put product success');
  });

  test('test delete product', () async {
    var response = await httpHelper.deleteProduct("1");
    var jsonBody = jsonDecode(response.body);
    expect(jsonBody['message'], "delete success");
  });
}
